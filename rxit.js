module.exports = function rxit(spec,func,timeout)
{
	timeout = timeout || 5000
	it(spec,function(done)
	{
		func().subscribe(function(){},function(err){if (err.message) done.fail(err.message); else done.fail(JSON.stringify(err));},function(){done();});
	},timeout)
}